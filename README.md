# Simplex

Trabajo práctico integrador de Investigación Operativa

Para acceder ingrese a: [https://simgraf-io.surge.sh/](https://simgraf-io.surge.sh/)

## Setup y Ejecución

- Instalar [Node.js](https://nodejs.org)
- Instalar yarn: `npm i -g yarn`
- Clonar repositorio: `git clone https://gitlab.com/leonisla22/io-tpi-svelte.git`
- Ir al directorio creado: `cd ./io-tpi-svelte`
- Instalar los paquetes: `yarn`
- Ejectuar: `yarn serve`
- Ir a [localhost:8080](http://localhost:8080)