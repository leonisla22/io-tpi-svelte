const solver = require("javascript-lp-solver");

model = {
  optimize: "objective",
  opType: "max",
  constraints: {
    r1: { min: 0 },
    r2: { min: 0 },
    r3: { min: 0 },
    r4: { min: 0 },
    r5: { max: 0 },
    r6: { max: 8 },
    r7: { max: 8 },
    r8: { max: 8 },
    r9: { max: 8 }
  },
  variables: {
    x1: {
      objective: 114.5,
      r1: 300,
      r2: 50,
      r3: 0,
      r4: 0,
      r5: 0,
      r6: 1 / 2000,
      r7: 0,
      r8: 0.55 / 2000,
      r9: 0.605 / 2500
    },
    x2: {
      objective: 105.03,
      r1: 0,
      r2: 0,
      r3: 0,
      r4: 0,
      r5: 0,
      r6: 0,
      r7: 1 / 500,
      r8: 0.97 / 2000,
      r9: 1.067 / 2500
    },
    x3: {
      objective: 103.03,
      r1: -50,
      r2: -2,
      r3: 0,
      r4: 0,
      r5: 0,
      r6: 0,
      r7: 1 / 500,
      r8: 0.97 / 2000,
      r9: 1.067 / 2500
    },
    x4: {
      objective: 94.5,
      r1: 0,
      r2: 0,
      r3: 0,
      r4: 30,
      r5: 10,
      r6: 1 / 2000,
      r7: 0,
      r8: 0.55 / 2000,
      r9: 0.605 / 2500
    },
    x5: {
      objective: 85.03,
      r1: 0,
      r2: 0,
      r3: -300,
      r4: -20,
      r5: -40,
      r6: 0,
      r7: 1 / 500,
      r8: 0.97 / 2000,
      r9: 1.067 / 2500
    },
    x6: {
      objective: 83.03,
      r1: 0,
      r2: 0,
      r3: -350,
      r4: -22,
      r5: -42,
      r6: 0,
      r7: 1 / 500,
      r8: 0.97 / 2000,
      r9: 1.067 / 2500
    }
  }
};

const results = solver.Solve(model);

console.log(results);
