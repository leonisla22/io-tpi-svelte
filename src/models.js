const objective = "objective";
const min = "min";
const max = "max";

let modelo1 = {
  optimize: objective,
  opType: max,
  constraints: {
    R1: { max: 3, resource: 3, sign: max },
    R2: { max: 6, resource: 6, sign: max },
    R3: { max: 36, resource: 36, sign: max },
    R4: { min: 1, resource: 1, sign: min },
    R5: { min: 1, resource: 1, sign: min }
  },
  variables: {
    x1: { objective: 8, R1: 1, R2: 0, R3: 6, R4: 1, R5: -1 },
    x2: { objective: 2, R1: 0, R2: 1, R3: 6, R4: 0, R5: 1 }
  }
};
let modelo2 = {
  optimize: objective,
  opType: min,
  constraints: {
    R1: { min: 10, resource: 10, sign: min },
    R2: { max: 5, resource: 5, sign: max },
    R3: { max: 1, resource: 1, sign: max }
  },
  variables: {
    x1: { objective: 2, R1: 1, R2: 2, R3: 3 },
    x2: { objective: 2, R1: 1, R2: -1, R3: 0 },
    x3: { objective: 1, R1: 1, R2: 0, R3: 0 }
  }
};

export const models = [modelo1, modelo2];
